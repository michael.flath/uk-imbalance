
library(tidyverse)
library(lubridate)
library(dbplyr)
library(xlsx)
library(optimaxload)
library(optimaxutils)



# in-sample period --------------------------------------------------------

start_date_in_sample <- ymd_hms("2019-01-01 00:00:00", tz = "CET")
end_date_in_sample <- ymd_hms("2021-01-01 00:00:00", tz = "CET")


# out-sample period -------------------------------------------------------

start_date_out_sample <- ymd_hms("2021-01-01 00:00:00", tz = "CET")
end_date_out_sample <- ymd_hms("2021-06-21 00:00:00", tz = "CET")



# data choice -------------------------------------------------------------
### choose 

# "in-sample"
# "out-sample"

data <- "in-sample"


### choose

## "model_1h_da_price_net_volume"
# fundamental data with offset 1h
# with day ahead price
# inter delta as net interconnector export-import volume

## "model_1h_net_volume"
# fundamental data with offset 1h
# without day ahead price
# inter delta as net interconnector export-import volume

## "model_2h_net_volume"
# fundamental data with offset 2h
# without day ahead price
# inter delta as net interconnector export-import volume

## "model_1h_net_volume_change"
# fundamental data with offset 1h
# without day ahead price
# inter delta as change in net interconnector export-import volume

model <- "model_1h_da_price_net_volume"


# sections ----------------------------------------------------------------

### in-sample

if(data == "in-sample"){
source("01_load_data_in_sample.R")

source("11_process_data_in_sample.R")
  
source("20_model.R")
  
source("31_backtest_in_sample.R")
}


### out-sample

if(data == "out-sample"){
source("02_load_data_out_sample.R")

source("12_process_data_out_sample.R")
  
source("32_backtest_out_sample.R")
}
