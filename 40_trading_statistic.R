
# UK trade distribution ---------------------------------------------------

trade_distribution <- trades %>%
  convert_time_columns_to_CET() %>%
  filter(type == 30) %>%
  mutate(
    time_to_del = round(difftime(delivery_start, execution_time, unit = "mins")),
    number_of_products = length(unique(delivery_start)),
    time_group = round(time_to_del / 10)
    ) %>%
  group_by(time_group) %>%
  mutate(trade_quantity = round(sum(quantity) / number_of_products)) %>%
  ungroup() %>%
  distinct(time_to_del, trade_quantity) %>%
  arrange(time_to_del)


ggplot(trade_distribution) +
  geom_col(aes(x = time_to_del, y = trade_quantity), fill = "darkorange", width = 1.1) +
  xlim(14, 120) +
  scale_y_continuous(n.breaks = 10) +
  xlab("Time to delivery") +
  ylab("Trade volume in MW") +
  ggtitle("Trade volume in dependency to time to delivery")
